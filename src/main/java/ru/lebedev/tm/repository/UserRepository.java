package ru.lebedev.tm.repository;

import ru.lebedev.tm.entity.User;
import ru.lebedev.tm.enumerated.RoleType;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private List<User> users = new ArrayList<>();

    public User create(final String firstName, final String middleName, final String secondName,
                       final String login, final String password, RoleType roleType) {
        final User user = new User(firstName);
        user.setMiddleName(middleName);
        user.setSecondName(secondName);
        user.setLogin(login);
        user.setPassword(password);
        user.setUserRole(roleType);
        users.add(user);
        return user;
    }

    public User update(final Long id, final String firstName, final String middleName, final String secondName,
                       final String login, RoleType roleType) {
        final User user = findById(id);
        if (user == null) return null;
        user.setId(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setSecondName(secondName);
        user.setLogin(login);
        user.setUserRole(roleType);
        return user;
    }

    public User update(final Long id, final String password){
        final User user = findById(id);
        if (user == null) return null;
        user.setPassword(password);
        return user;
    }

    public User update(final Long id, RoleType roleType){
        final User user = findById(id);
        if (user == null) return null;
        user.setUserRole(roleType);
        return user;
    }

    public void clear() {
         users.clear();
    }

    public int numberOfUsers() {
        return users.size();
    }

    public List<User> findAll() {
        return users;
    }

    public User findByIndex(final int index) {
        return users.get(index);
    }

    public User findByLogin(final String login) {
        for (final User user: users) {
            if(user.getLogin().equals(login)) return user;
        }
        return null;

    }

    public User findById(final Long id) {
        for (final User user: users) {
            if(user.getId().equals(id)) return user;
        }
        return null;

    }

    public User removeByIndex(final int index) {
        final User user = findByIndex(index);
        if (user == null) return null;
        users.remove(user);
        return user;

    }

    public User removeById(final Long id) {
        final User user = findById(id);
        if (user == null) return null;
        users.remove(user);
        return user;

    }

    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        users.remove(user);
        return user;

    }


}
