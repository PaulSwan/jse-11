package ru.lebedev.tm.controller;

import ru.lebedev.tm.entity.User;
import ru.lebedev.tm.enumerated.RoleType;
import ru.lebedev.tm.service.UserService;

import static ru.lebedev.tm.constant.TerminalConst.ADMINISTRATOR;
import static ru.lebedev.tm.constant.TerminalConst.SIMPLE_USER;

public class UserController extends AbstractController {
    private final UserService userService;

    public UserController(UserService userService){
        this.userService = userService;
    }

    public int createUser() {
        System.out.println("[CREATE USER]");
        System.out.println("PLEASE, ENTER USER FIRST NAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER SECOND NAME:");
        final String secondName = scanner.nextLine();

        String login;
        boolean isLogin;
        do {
            isLogin = false;
            System.out.println("PLEASE, ENTER USER LOGIN:");
            login = scanner.nextLine();
            if(null != userService.findByLogin(login)) {
                isLogin = true;
                System.out.println("LOGIN EXISTS!");
            }
        } while (isLogin);

        System.out.println("PLEASE, ENTER USER PASSWORD:");
        final String password = scanner.nextLine();
        System.out.println("PLEASE, ASSIGN A USER ROLE, ENTER:");
        System.out.println("admin - ADMINISTRATOR");
        System.out.println("user - SIMPLE USER");
        RoleType userRole;
        final String role = scanner.nextLine();
        switch(role){
            case ADMINISTRATOR: userRole = RoleType.ADMIN; break;
            case SIMPLE_USER: userRole = RoleType.USER; break;

            default: {
                System.out.println("Unexpected value: " + role);
                return -1;
            }

        }
        userService.create(firstName, middleName, secondName, login, password, userRole);
        System.out.println("[OK]");
        return 0;
    }

    public int updateUserByLogin() {
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER USER FIRST NAME:");
        final String firstName = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER MIDDLE NAME:");
        final String middleName = scanner.nextLine();
        System.out.println("PLEASE, ENTER USER SECOND NAME:");
        final String secondName = scanner.nextLine();
        userService.update(user.getId(), firstName, middleName, secondName, user.getLogin(),  user.getUserRole());
        System.out.println("[OK]");
        return 0;
    }

    public int userChangePasswordByLogin(){
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER NEW PASSWORD:");
        final String newPassword = scanner.nextLine();
        userService.update(user.getId(), newPassword);
        System.out.println("[OK]");
        return 0;
    }

    public int userChangeRoleByLogin() {
        System.out.println("[CHANGE USER ROLE]");
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER NEW ROLE");
        System.out.println("admin - ADMINISTRATOR");
        System.out.println("user - SIMPLE USER");
        RoleType userRole;
        final String role = scanner.nextLine();
        switch (role) {
            case ADMINISTRATOR:
                userRole = RoleType.ADMIN;
                break;
            case SIMPLE_USER:
                userRole = RoleType.USER;
                break;

            default: {
                System.out.println("Unexpected value: " + role);
                return -1;
            }
        }

        userService.update(user.getId(), userRole);
        System.out.println("[OK]");
        return 0;

    }

    public int clearUser() {
        System.out.println("[CLEAR USER]");
       userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int removeUserByLogin() {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("PLEASE, ENTER USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.removeByLogin(login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int removeUserById() {
        System.out.println("[REMOVE USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID:");
        final long id = scanner.nextLong();
        final User user = userService.removeById(id);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int removeUserByIndex() {
        System.out.println("[REMOVE USER BY INDEX]");
        System.out.println("PLEASE, ENTER USER INDEX:");
        final int index = scanner.nextInt() - 1;
        final User user = userService.removeByIndex(index);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public void viewUser(final User user) {
        if (user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("FULL NAME: " + user.getFirstName() + " " + user.getMiddleName() + " " + user.getSecondName());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("PASSWORD: " + user.getPassword());
        System.out.println("ROLE: " + user.getUserRole());
        System.out.println("[OK]");

    }

    public int listUser() {
        System.out.println("[LIST USER]");
        int index = 1;
        for (final User user: userService.findAll()) {
            System.out.println(index + ". |" + user.getId() + " | " + user.getFirstName() + " " + user.getMiddleName()
                               + " " + user.getSecondName() + " | " + user.getLogin() + " | " + user.getUserRole());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int viewUserByLogin() {
        System.out.println("ENTER, USER LOGIN:");
        final String login = scanner.nextLine();
        final User user = userService.findByLogin(login);
        viewUser(user);
        return 0;

    }


}
