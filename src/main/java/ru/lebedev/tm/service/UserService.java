package ru.lebedev.tm.service;

import ru.lebedev.tm.entity.User;
import ru.lebedev.tm.enumerated.RoleType;
import ru.lebedev.tm.repository.UserRepository;

import java.util.List;

public class UserService {

    private final UserRepository userRepository;


    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;

    }

    public User create(final String firstName, final String middleName, final String secondName,
                       final String login, final String password, RoleType roleType) {
        if (firstName == null || firstName.isEmpty()) return null;
        if (middleName == null || middleName.isEmpty()) return null;
        if (secondName == null|| secondName.isEmpty()) return null;
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        if (roleType.toString() == null) return null;
        String hPassword = hashPasswordMD5(password);

        return userRepository.create(firstName, middleName, secondName, login, hPassword, roleType);
    }

    public User update(final Long id, final String firstName, final String middleName, final String secondName,
                       final String login, RoleType roleType){
        if(id == null) return null;
        if(firstName == null || firstName.isEmpty()) return null;
        if(middleName == null || middleName.isEmpty()) return null;
        if(secondName == null || secondName.isEmpty()) return null;
        if (login == null || login.isEmpty()) return null;
        if (roleType.toString() == null) return null;
        return userRepository.update(id, firstName, middleName, secondName, login, roleType);
    }

    public User update(final Long id, final String password) {
        if (id == null) return null;
        if (password == null || password.isEmpty()) return null;
        String hPassword = hashPasswordMD5(password);

        return userRepository.update(id, hPassword);
    }

    public User update(final Long id, RoleType roleType) {
        if (id == null) return null;
        if (roleType.toString() == null) return null;
        return userRepository.update(id, roleType);
    }



        public String hashPasswordMD5(String password) {
          try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(password.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public void clear() {
        userRepository.clear();
    }

    public User findByIndex(int index) {
        if (index < 0 || index > userRepository.numberOfUsers() - 1) return null;
        return userRepository.findByIndex(index);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByLogin(String login) {
        if(login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    public User findById(Long id) {
        if(id == null) return null;
        return userRepository.findById(id);
    }

    public User removeByIndex(int index) {
        if (index < 0 || index > userRepository.numberOfUsers() - 1) return null;
        return userRepository.removeByIndex(index);
    }

    public User removeById(Long id) {
        if(id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.removeByLogin(login);
    }



}
